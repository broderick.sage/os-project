import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';

import entities from './entities';
import controllers from './controllers';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'db',
      port: 3306,
      username: 'root',
      password: 'example',
      database: 'Example',
      entities: Object.values(entities),
      synchronize: false
    }),
    ...Object.values(controllers)
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
