#!/usr/bin/env bash

TABLE_NAME=$1

function main {
    copyFiles
    addControllerImport
    addEntityImport
}

function copyFiles {
    # This function does 4 things:
    # 1. It creates the controllers and entities folders if they do not exist
    # 2. Creates a controller folder
    # 3. Copies the template files to the new folder and names them accordingly
    # 4. Finds and Replaces all instances of Template with $TABLE_NAME in the files

    # Create controllers and entity folds if they do not exist
    mkdir -p "../nestjs/src/controllers"
    mkdir -p "../nestjs/src/entities"

    if [ ! -f "../nestjs/src/controllers/index.ts" ]; then
        printf '\nexport default {\n}' > "../nestjs/src/controllers/index.ts"
    fi

    if [ ! -f "../nestjs/src/entities/index.ts" ]; then
        printf '\nexport default {\n}' > "../nestjs/src/entities/index.ts"
    fi
    

    # Create new controller folder for this table
    NEW_DIR="../nestjs/src/controllers/${TABLE_NAME,}"
    mkdir -p $NEW_DIR

    # For each of the files, copy the file to the controller folder and name it
    # accordingly
    for file in ./files/*; do
        cut_filename="$(echo $file | cut -c18-)" 
        cp "$file" "$NEW_DIR/${TABLE_NAME,}$cut_filename"
    done

    # For each of the files, find and replace instances of "template" with 
    # the new table name
    for file in $NEW_DIR/*; do
        sed -i "s/template/${TABLE_NAME,}/g" $file
        sed -i "s/Template/${TABLE_NAME^}/g" $file
    done
}

function addControllerImport {
    # This function does two things:
    # 1. Insert an import statement in controllers/index.ts for the new controller
    # 2. Add the new controller to the default export list

    # Set the file to be updated
    file="../nestjs/src/controllers/index.ts"
    new_line="import { ${TABLE_NAME^}Module } from './${TABLE_NAME,}/${TABLE_NAME,}.module';"

    # Add new controller to controllers/index.ts imports
    if ! grep -q "$new_line" $file; then
        sed -i "1i \\
        $new_line\\
        " $file
    fi

    # Add new controller to controllers/index.ts default exports
    if ! grep -q "\ ${TABLE_NAME^}Module," $file; then
        sed -i "/export/ a \\
        \    ${TABLE_NAME^}Module,\\
        " $file
    fi

}

function addEntityImport {
    # This function does two things:
    # 1. Insert an import statement in entities/index.ts for the new entitiy
    # 2. Add the new entity to the default export list

    # Set the file to be updated
    file="../nestjs/src/entities/index.ts"
    new_line="import { ${TABLE_NAME^} } from './$TABLE_NAME';"


    # Add new entity to entities/index.ts imports
    if ! grep -q "$new_line" $file; then
        sed -i "1i \\
        $new_line\\
        " $file
    fi

    # Add new entity to entities/index.ts default exports
    if ! grep -q "\ ${TABLE_NAME^}," $file; then
        sed -i "/export/ a \\
        \    ${TABLE_NAME^},\\
        " $file
    fi
}

main