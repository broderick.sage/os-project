#!/usr/bin/env bash

function main {
    # Generator models (entities)
    ../nestjs/node_modules/typeorm-model-generator/bin/typeorm-model-generator -h localhost -d Example -u root -x example -e mysql
    echo "Finished generating Entities."

    # For each entity, run template.sh to create the controller files and then copy to entities folder
    echo "Generating controllers."
    for file in ./output/entities/*; do
        TABLE_NAME=$(basename ${file%.ts})
        NEW_ENTITY=../nestjs/src/entities/${TABLE_NAME}.ts 
        ./template.sh $TABLE_NAME
        cp "$file" "$NEW_ENTITY"
    done
    echo "Finished generating controllers."

    # Remove output files now that they've been copied to correct location
    echo "Removing typeorm generated files."
    rm -r output
    echo "Removed typeorm generated files."

    # Run entityJoins.sh to add first level of joins to entities. 
    # NOTE: Currently nested joins must be added manually.
    echo "Adding first level joins to entities."
    entity_joins
    echo "Generate.sh has completed."
}

function entity_joins {
  for file in ../nestjs/src/entities/*; do
      TABLE=$(basename ${file%.ts})
      if [[ "$TABLE" != "index" ]]; then
        controller="../nestjs/src/controllers/${TABLE,}/${TABLE,}.controller.ts"
        matches=$(grep -A 20 '@OneToMany' $file | sed -En 's/.* ([^ ]*): [^"].*/\1/p' && grep -A 20 '@ManyToOne' $file | sed -En 's/.* ([^ ]*): [^"].*/\1/p')

        for match in $matches; do
            sed -i -E 's/(join:.*$)/\1\
          '"$match"': {eager: false},/g' $controller
        done
      fi
  done
}

main