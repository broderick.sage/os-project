import { Controller } from '@nestjs/common';
import { Crud } from '@nestjsx/crud';

import { TemplateService } from './template.service';
import { Template } from '../../entities/Template';

@Crud({
  model: {
    type: Template,
  },
  query: {
    join: {
    }
  }
})
@Controller('template')
export class TemplateController {
  constructor(public service: TemplateService) {}
}
