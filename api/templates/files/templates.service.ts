import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';

import { Template } from '../../entities/Template';

@Injectable()
export class TemplateService extends TypeOrmCrudService<Template> {
  constructor(
    @InjectRepository(Template)
    private readonly templateRepository: Repository<Template>
  ) {
    super(templateRepository);
  }
}
