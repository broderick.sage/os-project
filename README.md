# Introduction

This repo is my project for Operating Systems. My goal for this project was to create a system that would allow a user to provide a
.sql file and would generate an entire backend with CRUD endpoints. For this I wanted the the only dependency to be that the user 
would need to have Docker installed. The rest would be generated and stood up inside of an API container and a Database container.

# Development

There are several pieces that are already created for this project. [NestJS][1] provides the basis for the API. Building off that I connected it to the mysql database running in the other container. I also set up the basis for using swagger (used to test the API), crud calls, and TypeORM entities. [Typeorm-model-generator][2] can build these entities by connecting to the database. Once this was set up the main part of the work started.

There was two main things that needed to be generated, the controller files for each table, and the joins based off the entities. This work is reflected in the api/templates folder. Specifically in two scripts: generate.sh and template.sh.

The first script, generate.sh, starts by calling the typeorm-model-generator and creating the entities. It then steps through each of the generated entity files and passes it over to the second script, template.sh.  This script takes the entity and copies each of the template controller files over with the correct name. It then uses sed to run through the file and update all instances of the template name to match the new entity. It also handles things like ensuring the controller directory exists and that the new controller has been added to the index file. It then passes it back to the generate script, which adds corresponding entity joins for each of the foreign keys.

Aside from that, all that's left is the start script which runs all this and prompts the user for their db file. Upon running ./start.sh the containers are built and run. From there the user can access the swagger tester at localhost:3000/api .

# Use

In order to use the project, you first must have [Docker][3] installed. Then clone (or download) this repo to a local folder. run `./start.sh` from the terminal and provide the path to your .sql file (or leave blank to use the example included) and hit enter. This will build the containers and run them.

From there everything is set up. You can access swagger at http://localhost:3000

If you are using the example sql file, two example urls are:
[http://localhost:3000/table1?join=subTable&join=subTable2]
[http://localhost:3000/api/#/default/getManyBaseTable1ControllerTable1]

For the second one, you can add subTable and subTable2 to the join list and see how it changes.

# Further Work

There is alot more I wished to do with this, but due to time constraints got cut. Originally I had hoped to also create a basic front end ui using angular with generated pages for each table. Nothing fancy, but basic content with options to view, update, delete, etc. 

Another improvement would be to include nested fields in the api. Right now the joins must be listed in the url, however NestJS also allows for a user to include these fields by default. However that would involve tracing through the database and being able to tell where to start. Still. This would make this alot better and I intend to pursue it.

Oh and lastly I would have liked to expand this using only mysql. Multiple database file formats are supported, but I would need to run the appropriate container and make various updates through out for that to work. Still, a very worthwhile goal.

# Conclusion

This project was a lot of fun and I learned a lot about docker and docker-compose while doing it. Not to mention multiple terminal commands, such as sed, and a intense refresher on RegEx.

# Resources

This project makes use of the following technologies:
[Typeorm-model-generator][2]
[nestJS][1]
[Docker (and docker-compose)][3]
[mySQL (and mySQL workbench for testing)][4]
[swagger (for testing the endpoints)][5]


[1]: https://nestjs.com/
[2]: https://www.npmjs.com/package/typeorm-model-generator
[3]: https://www.docker.com/
[4]: https://www.mysql.com/
[5]: https://swagger.io/