#!/bin/bash

read -e -p "Please provide the .sql file to be used (or leave blank to use the provided example): " file
file=${file:-"./default.sql"}

cp $file ./db/init/db.sql

docker-compose build --no-cache db
docker-compose up db &

# This kinda defeats the point of using docker-compose, 
#   however in order to generate the entity files, 
#   this container needs to be able to connect to the db.
# However I want to avoid forcing the user to need anything except docker.
docker-compose build --no-cache api
docker-compose up api &

echo "From now on, dc up can be used to start this."