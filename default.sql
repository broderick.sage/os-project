CREATE DATABASE  IF NOT EXISTS `Example` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `Example`;
-- MySQL dump 10.13  Distrib 8.0.15, for macos10.14 (x86_64)
--
-- Host: 127.0.0.1    Database: Example
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `SubTable`
--

DROP TABLE IF EXISTS `SubTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `SubTable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `SubTable_value` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SubTable`
--

LOCK TABLES `SubTable` WRITE;
/*!40000 ALTER TABLE `SubTable` DISABLE KEYS */;
INSERT INTO `SubTable` VALUES (1,'red'),(2,'blue'),(3,'green'),(4,'gold'),(5,'silver');
/*!40000 ALTER TABLE `SubTable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SubTable2`
--

DROP TABLE IF EXISTS `SubTable2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `SubTable2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `SubTable2_value` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SubTable2`
--

LOCK TABLES `SubTable2` WRITE;
/*!40000 ALTER TABLE `SubTable2` DISABLE KEYS */;
INSERT INTO `SubTable2` VALUES (1,'circle'),(2,'square'),(3,'triangle'),(4,'oval'),(5,'rectangle');
/*!40000 ALTER TABLE `SubTable2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Table1`
--

DROP TABLE IF EXISTS `Table1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Table1` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SubTableID` int(11) DEFAULT NULL,
  `SubTable2ID` int(11) DEFAULT NULL,
  `Table1_value` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK1_idx` (`SubTableID`),
  KEY `FK2_idx` (`SubTable2ID`),
  CONSTRAINT `FK1` FOREIGN KEY (`SubTableID`) REFERENCES `SubTable` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK2` FOREIGN KEY (`SubTable2ID`) REFERENCES `SubTable2` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Table1`
--

LOCK TABLES `Table1` WRITE;
/*!40000 ALTER TABLE `Table1` DISABLE KEYS */;
INSERT INTO `Table1` VALUES (1,1,1,'value 1'),(2,1,2,'value 2'),(3,2,3,'value 3'),(4,4,4,'value 4'),(5,3,5,'value 5'),(6,5,2,'value 6');
/*!40000 ALTER TABLE `Table1` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-30  0:31:42
